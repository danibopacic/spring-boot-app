DROP TABLE IF EXISTS myorderdrink;
DROP TABLE IF EXISTS myorder;
DROP TABLE IF EXISTS drink;
DROP TABLE IF EXISTS mytable;
CREATE TABLE Drink (
    id SERIAL PRIMARY KEY,
    name VARCHAR(255),
    price DOUBLE PRECISION
);


CREATE TABLE MyTable(
    id BIGINT PRIMARY KEY,
    name VARCHAR(255)
);

CREATE TABLE MyOrder (
    id SERIAL PRIMARY KEY,
    table_id INT REFERENCES MyTable(id),
    comment VARCHAR(255),
    status VARCHAR(255)
);

CREATE TABLE MyOrderDrink (
    id SERIAL PRIMARY KEY,
    myorder_id INT REFERENCES MyOrder(id),
    drink_id INT REFERENCES Drink(id),
    quantity INT
);


INSERT INTO Drink (name, price) VALUES
    ('Espresso', 1.5),
    ('Cappuccino', 2.0),
    ('Macchiato', 1.8),
    ('Latte', 1.8),
    ('Osjecko', 2.0),
    ('Coca Cola', 2.5),
    ('Pepsi', 2.5),
    ('Fanta', 2.5);

INSERT INTO MyTable (id, name) VALUES
    (5, 'Table #5'),
    (69, 'Table #69'),
    (144, 'Table #144');


INSERT INTO MyOrder (table_id, comment, status) VALUES
    (5, 'With sugar please', 'NEW'),
    (69, 'With sugar please', 'NEW'),
    (144, 'No sugar please', 'NEW');

INSERT INTO MyOrderDrink(myorder_id, drink_id, quantity) VALUES
    (1, 1, 2),
    (1, 2, 1),
    (2, 1, 2),
    (2, 2, 3),
    (2, 4, 5);

//Upiti:

//Dohvacanje svih itema za pojedinu narudzbu:
 select * from myorder AS mo 
    INNER JOIN myorderdrink AS mod ON mo.id = mod.myorder_id 
    INNER JOIN drink AS d on mod.drink_id=d.id 
    WHERE mo.id = 1;


//Dohvacanje cijene narudzbe po itemima

 select d.name, mod.quantity, (d.price * mod.quantity) as total from myorder AS mo 
    INNER JOIN myorderdrink AS mod ON mo.id = mod.myorder_id 
    INNER JOIN drink AS d on mod.drink_id=d.id 
    WHERE mo.id = 1;

//Ukupna cijena

select sum(sq.total) FROM (select d.name, mod.quantity, (d.price * mod.quantity) as total from myorder AS mo 
    INNER JOIN myorderdrink AS mod ON mo.id = mod.myorder_id 
    INNER JOIN drink AS d on mod.drink_id=d.id 
    WHERE mo.id = 1) as sq;