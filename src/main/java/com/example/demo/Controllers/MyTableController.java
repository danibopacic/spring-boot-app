package com.example.demo.Controllers;


import com.example.demo.Entitites.MyTable;
import com.example.demo.Service.MyTableService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("mytable")
public class MyTableController {

    private final MyTableService myTableService;

    @Autowired
    public MyTableController(MyTableService myTableService){this.myTableService=myTableService;}

    @GetMapping("/getAll")
    public List<MyTable> getAllMyTables(){
        return this.myTableService.getAllMyTables();
    }
}
