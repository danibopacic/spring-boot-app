package com.example.demo.Controllers;


import com.example.demo.DTO.OrderedDrink;
import com.example.demo.Entitites.MyOrder;
import com.example.demo.Entitites.MyOrderDrink;
import com.example.demo.Service.MyOrderDrinkService;
import com.example.demo.Service.MyOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("myorderdrink")
public class MyOrderDrinkController {

    private final MyOrderDrinkService myOrderDrinkService;

    @Autowired
    public MyOrderDrinkController(MyOrderDrinkService myOrderDrinkService){this.myOrderDrinkService=myOrderDrinkService;}

    @GetMapping("/getAll")
    public List<MyOrderDrink> getAllMyOrderDrinks(){
        return this.myOrderDrinkService.getAllMyOrderDrinks();
    }

    @GetMapping("/getOrderedDrinks/{id}")
    public List<OrderedDrink> getOrderedItemsForOrder(@PathVariable("id") Long id) {
        return this.myOrderDrinkService.getOrderedItemsForOrder(id);
    }
}
