package com.example.demo.Controllers;


import com.example.demo.Entitites.Drink;
import com.example.demo.Service.DrinkService;
import com.example.demo.DTO.DrinkDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("drink")
public class DrinkController {

    private final DrinkService drinkService;

    @Autowired
    public DrinkController(DrinkService drinkService){this.drinkService=drinkService;}

    @GetMapping("/getAll/{table_id}")
    public DrinkDTO getAllDrinks(@PathVariable("table_id") Long id){
        List<Drink> drinkList = this.drinkService.getAllDrinks();
        return new DrinkDTO(id, drinkList);
    }


}
