package com.example.demo.Controllers;


import com.example.demo.DTO.MyOrderMobileResponse;
import com.example.demo.DTO.OrderDTO;
import com.example.demo.Entitites.MyOrder;
import com.example.demo.Service.MyOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("myorder")
public class MyOrderController {

    private final MyOrderService myOrderService;

    @Autowired
    public MyOrderController(MyOrderService myOrderService){this.myOrderService=myOrderService;}

    @GetMapping("/getAll")
    public List<MyOrder> getAllMyOrders(){
        return this.myOrderService.getAllMyOrders();
    }

    @GetMapping("/{id}")
    public Optional<MyOrder> getAllMyOrders(@PathVariable("id") Long id){
        return this.myOrderService.getmyOrderStatus(id);
    }

    @PutMapping("/{id}")
    public MyOrder updateMyOrderStatus(@PathVariable("id") Long id, @RequestBody MyOrder updatedOrder){
        return this.myOrderService.updatemyOrderStatus(id, updatedOrder);
    }

    @PostMapping
    public MyOrderMobileResponse createMyOrder(@RequestBody OrderDTO orderDTO){
        return this.myOrderService.createMyOrder(orderDTO);
    }
}
