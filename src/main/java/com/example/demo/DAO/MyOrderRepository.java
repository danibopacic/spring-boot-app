package com.example.demo.DAO;

import com.example.demo.DTO.OrderDTO;
import com.example.demo.Entitites.MyOrder;
import com.example.demo.Entitites.MyTable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface MyOrderRepository extends JpaRepository<MyOrder, Long> {

}
