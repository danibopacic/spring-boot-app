package com.example.demo.DAO;

import com.example.demo.DTO.OrderedDrink;
import com.example.demo.Entitites.MyOrder;
import com.example.demo.Entitites.MyOrderDrink;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface MyOrderDrinkRepository extends JpaRepository<MyOrderDrink, Long> {

    @Query("SELECT new com.example.demo.DTO.OrderedDrink(mod.drink.id, mod.quantity, d.name, d.price) FROM MyOrder mo " +
            "INNER JOIN MyOrderDrink mod ON mo.id = mod.myOrder.id " +
            "INNER JOIN Drink d ON mod.drink.id = d.id " +
            "WHERE mo.id = :orderId")
    List<OrderedDrink> getOrderedItemsForOrder(@Param("orderId") Long orderId);
}
