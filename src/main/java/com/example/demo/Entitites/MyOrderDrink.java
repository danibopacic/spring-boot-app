package com.example.demo.Entitites;


import jakarta.persistence.*;

@Entity(name = "MyOrderDrink")
@Table(name = "myorderdrink")
public class MyOrderDrink {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    @JoinColumn(name = "myorder_id")
    private MyOrder myOrder;

    @ManyToOne
    @JoinColumn(name = "drink_id")
    private Drink drink;

    private Integer quantity;

    // Constructors
    public MyOrderDrink() {}

    public MyOrderDrink(Long id, MyOrder myOrder, Drink drink, Integer quantity) {
        this.id = id;
        this.myOrder = myOrder;
        this.drink = drink;
        this.quantity = quantity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MyOrder getMyOrder() {
        return myOrder;
    }

    public void setMyOrder(MyOrder myOrder) {
        this.myOrder = myOrder;
    }

    public Drink getDrink() {
        return drink;
    }

    public void setDrink(Drink drink) {
        this.drink = drink;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "MyOrderDrink{" +
                "id=" + id +
                ", myOrder=" + myOrder +
                ", drink=" + drink +
                ", quantity=" + quantity +
                '}';
    }
}

