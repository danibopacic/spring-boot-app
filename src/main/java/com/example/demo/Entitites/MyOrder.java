package com.example.demo.Entitites;


import jakarta.persistence.*;

@Entity(name = "MyOrder")
@Table(name = "myorder")
public class MyOrder {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;


    @ManyToOne
    @JoinColumn(name = "table_id")
    private MyTable myTable;

    private String comment;

    private String status;

    // Constructors
    public MyOrder() {}

    public MyOrder(MyTable myTable, String comment, String status) {
        this.myTable = myTable;
        this.comment = comment;
        this.status = status;
    }

    public MyOrder(MyOrder other) {
        id = other.id;
        myTable = other.myTable;
        comment = other.comment;
        status = other.status;
    }

    // Getters and Setters
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public MyTable getMyTable() {
        return myTable;
    }

    public void setMyTable(MyTable myTable) {
        this.myTable = myTable;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }


    @Override
    public String toString() {
        return "MyOrder{" +
                "id=" + id +
                ", myTable=" + myTable +
                ", comment='" + comment + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}

