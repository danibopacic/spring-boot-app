package com.example.demo.DTO;

public class MyOrderMobileResponse {

    private Long myOrderId;

    private String status;

    public MyOrderMobileResponse() {
    }

    public MyOrderMobileResponse(Long myOrderId, String status) {
        this.myOrderId = myOrderId;
        this.status = status;
    }

    public Long getMyOrderId() {
        return myOrderId;
    }

    public void setMyOrderId(Long myOrderId) {
        this.myOrderId = myOrderId;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "MyOrderMobileResponse{" +
                "myOrderId=" + myOrderId +
                ", status='" + status + '\'' +
                '}';
    }
}
