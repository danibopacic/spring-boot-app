package com.example.demo.DTO;

import com.example.demo.Entitites.Drink;

import java.util.List;

public class DrinkDTO {
    public Long tableId;
    public List<Drink> drinks;

    public DrinkDTO(){

    }

    public DrinkDTO(Long tableId, List<Drink> drinks){
        this.tableId = tableId;
        this.drinks = drinks;
    }
}
