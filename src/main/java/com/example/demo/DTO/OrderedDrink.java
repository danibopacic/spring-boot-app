package com.example.demo.DTO;

public class OrderedDrink {

    private Long drinkId;
    private Integer quantity;
    private String name;
    private Double price;

    public OrderedDrink() {
    }

    public OrderedDrink(Long drinkId, Integer quantity, String name, Double price) {
        this.drinkId = drinkId;
        this.quantity = quantity;
        this.name = name;
        this.price = price;
    }

    public Long getDrinkId() {
        return drinkId;
    }

    public void setDrinkId(Long drinkId) {
        this.drinkId = drinkId;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return "OrderedDrink{" +
                "drinkId=" + drinkId +
                ", quantity=" + quantity +
                ", name='" + name + '\'' +
                ", price=" + price +
                '}';
    }
}
