package com.example.demo.DTO;

import java.util.List;

public class OrderDTO {

    private Long tableId;

    private List<OrderedDrink> orderedDrinks;

    private String comment;

    private String status;

    public OrderDTO() {
    }

    public OrderDTO(Long tableId, List<OrderedDrink> orderedDrinks, String comment, String status) {
        this.tableId = tableId;
        this.orderedDrinks = orderedDrinks;
        this.comment = comment;
        this.status = status;
    }

    public Long getTableId() {
        return tableId;
    }

    public void setTableId(Long tableId) {
        this.tableId = tableId;
    }

    public List<OrderedDrink> getOrderedDrinks() {
        return orderedDrinks;
    }

    public void setOrderedDrinks(List<OrderedDrink> orderedDrinks) {
        this.orderedDrinks = orderedDrinks;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "OrderDTO{" +
                "tableId=" + tableId +
                ", orderedDrinks=" + orderedDrinks +
                ", comment='" + comment + '\'' +
                ", status='" + status + '\'' +
                '}';
    }
}
