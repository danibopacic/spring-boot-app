package com.example.demo.Service;

import com.example.demo.DAO.DrinkRepository;
import com.example.demo.Entitites.Drink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class DrinkService {

    private final DrinkRepository drinkRepository;

    @Autowired
    public DrinkService(DrinkRepository drinkRepository){this.drinkRepository=drinkRepository;}


    public List<Drink> getAllDrinks() {
        return this.drinkRepository.findAll();
    }
}
