package com.example.demo.Service;

import com.example.demo.DAO.DrinkRepository;
import com.example.demo.DAO.MyTableRepository;
import com.example.demo.Entitites.Drink;
import com.example.demo.Entitites.MyTable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyTableService {

    private final MyTableRepository myTableRepository;

    @Autowired
    public MyTableService(MyTableRepository myTableRepository){this.myTableRepository=myTableRepository;}


    public List<MyTable> getAllMyTables() {
        return this.myTableRepository.findAll();
    }
}
