package com.example.demo.Service;

import com.example.demo.DAO.MyOrderDrinkRepository;
import com.example.demo.DAO.MyOrderRepository;
import com.example.demo.DTO.OrderedDrink;
import com.example.demo.Entitites.MyOrder;
import com.example.demo.Entitites.MyOrderDrink;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MyOrderDrinkService {

    private final MyOrderDrinkRepository myOrderDrinkRepository;

    @Autowired
    public MyOrderDrinkService(MyOrderDrinkRepository myOrderDrinkRepository){this.myOrderDrinkRepository=myOrderDrinkRepository;}


    public List<MyOrderDrink> getAllMyOrderDrinks() {
        return this.myOrderDrinkRepository.findAll();
    }

    public List<OrderedDrink> getOrderedItemsForOrder(Long id) {
        return this.myOrderDrinkRepository.getOrderedItemsForOrder(id);
    }
}
