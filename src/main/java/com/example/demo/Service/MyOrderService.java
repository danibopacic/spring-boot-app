package com.example.demo.Service;

import com.example.demo.DAO.MyOrderRepository;
import com.example.demo.DTO.MyOrderMobileResponse;
import com.example.demo.DTO.OrderDTO;
import com.example.demo.DTO.OrderedDrink;
import com.example.demo.Entitites.MyOrder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.jdbc.support.KeyHolder;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;
import java.util.Optional;

@Service
public class MyOrderService {

    private final MyOrderRepository myOrderRepository;
    @Autowired
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public MyOrderService(MyOrderRepository myOrderRepository){this.myOrderRepository=myOrderRepository;}


    public List<MyOrder> getAllMyOrders() {
        return this.myOrderRepository.findAll();
    }
    public Optional<MyOrder> getmyOrderStatus(Long id) {
        return this.myOrderRepository.findById(id);
    }

    public MyOrder updatemyOrderStatus(Long id, MyOrder updatedOrder){
        MyOrder myOrder = myOrderRepository.findById(id).orElse(null);

        if(myOrder != null){
            myOrder = new MyOrder(updatedOrder);
            myOrderRepository.save(myOrder);
        }
        return myOrder;
    }

    public MyOrderMobileResponse createMyOrder(OrderDTO orderDTO) {
        String query1 = "INSERT INTO myorder (table_id, comment, status) VALUES (?, ?, ?)";
        KeyHolder keyHolder = new GeneratedKeyHolder();
        jdbcTemplate.update(new PreparedStatementCreator() {
            @Override
            public PreparedStatement createPreparedStatement(Connection con) throws SQLException {
                PreparedStatement ps = con.prepareStatement(query1, Statement.RETURN_GENERATED_KEYS);
                ps.setLong(1, orderDTO.getTableId());
                ps.setString(2, orderDTO.getComment());
                ps.setString(3, orderDTO.getStatus());
                return ps;
            }
        }, keyHolder);

        MyOrderMobileResponse myOrderMobileResponse = new MyOrderMobileResponse();
        if(keyHolder.getKeys() == null) {
            myOrderMobileResponse.setMyOrderId(-1L);
            myOrderMobileResponse.setStatus("ERROR");
        } else {
            Long id = ((Number) keyHolder.getKeys().get("id")).longValue();
            myOrderMobileResponse.setMyOrderId(id);
            myOrderMobileResponse.setStatus("NEW");
            //INSERT myorder_id with drink_id and quantities into myorderdrink;
            String query2 = "INSERT INTO myorderdrink (myorder_id, drink_id, quantity) VALUES (?, ?, ?)";
            for(OrderedDrink orderedDrink : orderDTO.getOrderedDrinks()){
                jdbcTemplate.update(query2, myOrderMobileResponse.getMyOrderId(), orderedDrink.getDrinkId(), orderedDrink.getQuantity());
            }
        }
        return myOrderMobileResponse;
    }
}
